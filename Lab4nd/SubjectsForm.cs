﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;

namespace Lab4nd
{
    public partial class SubjectsForm : Form
    {
        private TestDBEntities context = new TestDBEntities();
        public SubjectsForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            context.Subject.Load();
            subjectBindingSource.DataSource = context.Subject.Local.ToBindingList();
        }

        private void subjectBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            Validate();
            subjectBindingSource.EndEdit();
            context.SaveChanges();
        }

        private void subjFilter_Click(object sender, EventArgs e)
        {
            var filteredData = context.Subject.Local.Where(x => x.Name.Contains(subjNameText.Text));
            subjectBindingSource.DataSource = filteredData;
        }

        private void clearSubjFilter_Click(object sender, EventArgs e)
        {
            subjNameText.Clear();
            subjectBindingSource.DataSource = context.Subject.Local.ToBindingList();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            context.Dispose();
        }
    }
}