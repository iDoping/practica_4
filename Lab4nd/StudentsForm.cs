﻿using System;
using System.Windows.Forms;
using System.Data.Entity;
using System.Linq;

namespace Lab4nd
{
    public partial class StudentsForm : Form
    {
        private TestDBEntities context = new TestDBEntities();
        public StudentsForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            context.StudentList.Load();
            studentListBindingSource.DataSource = context.StudentList.Local.ToBindingList();
        }

        private void studentListBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            Validate();
            studentListBindingSource.EndEdit();
            context.SaveChanges();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            context.Dispose();
        }

        private void filter_Click(object sender, EventArgs e)
        {
            var filteredData = context.StudentList.Local.Where(x => x.Surname.Contains(surnameText.Text));
            studentListBindingSource.DataSource = filteredData;
        }

        private void clearFilter_Click(object sender, EventArgs e)
        {
            surnameText.Clear();
            studentListBindingSource.DataSource = context.StudentList.Local.ToBindingList();
        }

        private void subjShow_Click(object sender, EventArgs e)
        {
            SubjectsForm form = new SubjectsForm();
            form.Show();
        }
    }
}